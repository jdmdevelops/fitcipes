<script>
  import Option from "./Nav/Options";
  import Link from "./Link";
  import hat from "../assets/chef-hat.svg";
</script>

<style>
  nav {
    display: flex;
    height: 50px;
    justify-content: center;
    align-items: center;
    margin-top: 10px;
    margin-bottom: 50px;
  }
  /* .menu {
    display: flex;
  } */
  header {
    color: white;
    font-weight: bold;
    font-size: 45px;
  }
</style>

<nav>
  <header>
    <Link name="Home">
      Fitcipes
      <img src={hat} alt="hat" width="40" />
    </Link>
  </header>

  <!-- <div class="menu">
    <Option name="Browse" />
  </div> -->
</nav>
