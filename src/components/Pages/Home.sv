<script>
  import { flip } from "svelte/animate";
  import Link from "../Link";
  import Recipe from "./Recipe";
  import recipesData from "../../assets/recipes.json";

  const recipes = recipesData.recipes;
</script>

<style>
  .recipes {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }
</style>

<div class="recipes">
  {#each recipes as recipe}
    <Recipe {recipe} />
  {/each}
</div>
