<script>
  import { fade, blur, fly, slide, scale, dra } from "svelte/transition";
  import { quintOut } from "svelte/easing";
  import { active } from "../stores.js";

  export let recipe;

  let clicked = false;

  function click(e) {
    clicked = !clicked;
    $active = !$active;
  }
</script>

<style>
  .recipe {
    width: 460px;
    background-color: #484848;
    padding: 5px;
    margin: 5px;
    display: inline-block;
  }
  .recipe:hover {
    cursor: pointer;
  }
  .active {
    width: 100%;
  }
  img {
    width: 100%;
    height: 300px;
    object-fit: cover;
    object-position: 50% 50%;
  }

  .info {
    text-align: center;
    font-weight: 300;
    background-color: #1a1a1a;
    padding: 10px;
    font-size: 20px;
    margin-top: 0px;
  }
  .title {
    font-size: 30px;
    text-align: center;
    font-weight: 500;
  }
  .clicked {
    margin-top: 10px;
  }
  .ingredients,
  .directions {
    text-align: left;
  }
</style>

{#if (!clicked && !$active) || (clicked && active)}
  <div
    class="recipe"
    class:active={clicked}
    on:click={click}
    in:fly={{ delay: 250, duration: 500, x: 0, y: 750, opacity: 0.3, easing: quintOut }}>
    <img src={recipe.image_url} alt="recipe image" />

    <div class="info">
      <div class="title">{recipe.title}</div>
      <div class="nutrition">
        <div class="calories">Calories: {recipe.calories}</div>
        <div class="macros">
          <div class="macro">Protein: {recipe.macros.protein}g</div>
          <div class="macro">Carbs: {recipe.macros.carbs}g</div>
          <div class="macro">Fat: {recipe.macros.fat}g</div>
          {#if clicked}
            <div class="clicked">
              <div class="calories">Serving size: {recipe.servingSize}</div>
              <div class="time-to-prepare">
                Prep-time: {recipe.prepTime} minutes
              </div>
              <ul class="ingredients">
                {#each recipe.ingredients as ingredient}
                  <li>{ingredient}</li>
                {/each}
              </ul>
              <ol class="directions">
                {#each recipe.directions as step}
                  <li>{step}</li>
                {/each}
              </ol>
            </div>
          {/if}
        </div>
      </div>
    </div>
  </div>
{/if}
