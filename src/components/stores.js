import { writable } from "svelte/store";

export const component = writable("Home");
export const active = writable(false);
