<script>
  import { component } from "./stores.js";
  export let name;
  export let page;
</script>

{#if $component == name}
  <svelte:component this={page} />
{/if}
