<script>
  import { component } from "./stores.js";

  export let name;

  function setName(name) {
    $component = name;
  }
</script>

<style>
  a {
    text-decoration: none;
  }
  a:visited {
    color: inherit;
  }
</style>

<a
  href="#/"
  on:click={e => {
    setName(name);
  }}>
  <slot>{name}</slot>
</a>
