<script>
  import Nav from "./Nav";
  import Content from "./Content";
  //   import Create from "./Pages/Create";
  import Home from "./Pages/Home";
  import Recipe from "./Pages/Recipe";
</script>

<style>
  @import url("https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&display=swap");
  :global(body) {
    background-color: black;
    font-family: "Montserrat", sans-serif;
    color: white;
    margin-left: 50px;
    margin-right: 50px;
    overflow: scroll;
  }
</style>

<Nav />
<Content name="Home" page={Home} />
<Content name="Recipe" page={Recipe} />
<!-- <Content name="Create" page={Create} /> -->
